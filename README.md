![Build Status](https://gitlab.com/samdbeckham/vuepress-gl-pages/badges/master/build.svg)

## Usefull links to start

* [MarkDown language](https://docs.gitlab.com/ee/user/markdown.html)
* [Additional features from VuePress](https://vuepress.vuejs.org/guide/markdown.html)

## About the technical background here
---

Example [VuePress][project] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:9.11.1

pages:
  cache:
    paths:
    - node_modules/

  script:
  - yarn install
  - yarn build

  artifacts:
    paths:
    - public
  
  only:
  - master

```

This sets up a `node9.11.1` environment, then uses `yarn install` to install dependencies and `yarn build` to build out the website to the `./public` directory.
It also caches the `node_modules` directory to speed up sebsequent builds.

## Building locally

This project uses [yarn](https://yarnpkg.com), you'll need to install this globally before you can get started. 

```
npm install -g yarn
```

Then you need to install the project dependencies:

```
yarn install
```

Now you're ready to go.
To run the local dev server just use the following command:

```
yarn start
```

Your website should be available at [http://localhost:8080/support]

*Read more at VuePress' [documentation](https://vuepress.vuejs.org/).*
