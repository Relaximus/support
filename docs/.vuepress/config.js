const dirTree = require('directory-tree');
const path = require('path');

let renderSideBar = (pathToSearch) => {
    const dir = dirTree(path.join(__dirname, '../'+pathToSearch), {extensions:/\.md/,exclude:/images/});
    let guideSidebar = dir.children.map(children => path.parse(children.name).name !== 'README' ? path.parse(children.name).name : '' );
    console.log(guideSidebar);
    return guideSidebar;
};

module.exports = {
    title: 'Avax User Support',
    description: 'The website containing the documents, describing user behavior with different parts of AVAX app.',
    base: '/support/',
    dest: 'public',
    serviceWorker: true,
    markdown: {
      config: _ => {
          _.use(require('markdown-it-html5-embed'),{
              html5embed: {
                  useImageSyntax: true,
                  useLinkSyntax: false
              }
          })
      }
    },
    themeConfig: {
        nav: [
            { text: 'GUIDE', link: '/guide/' },
            {text: 'NEWS', link: '/news/'}
        ],
        sidebar: {
            '/guide/': renderSideBar('guide'),
            '/news/': renderSideBar('news'),
              // fallback
              '/': ['','about']
        },
        search: true,
        searchMaxSuggestions: 10
  }
}